//
//  UIViewController+Progress.m
//  ProgressCell
//
//  Created by sudeep on 02/05/15.
//  Copyright (c) 2015 Sudeep Jaiswal. All rights reserved.
//

#import "UIViewController+Progress.h"
#import "ASJProgressCell.h"
#import <objc/runtime.h>

@interface UIViewController (Progress_Private)

@property (nonatomic) NSDictionary *progresses;

@end

@implementation UIViewController (Progress_Private)

- (NSDictionary *)progresses {
    return objc_getAssociatedObject(self, @selector(progresses));
}

- (void)setProgresses:(NSDictionary *)progresses {
    objc_setAssociatedObject(self, @selector(progresses), progresses, OBJC_ASSOCIATION_RETAIN);
}

@end

@implementation UIViewController (Offsets)

- (void)saveProgressForCell:(ASJProgressCell *)cell {
    if (!self.progresses) {
        self.progresses = [[NSDictionary alloc] init];
    }
    NSMutableDictionary *mDict = self.progresses.mutableCopy;
    NSNumber *progress = @(cell.aProgressView.progress);
    [mDict setObject:progress forKey:@(cell.tag)];
    self.progresses = [NSDictionary dictionaryWithDictionary:mDict];
    cell.aProgressView.progress = 0.0;
    cell.aLabel.text = 0;
}

- (CGFloat)progressForCell:(ASJProgressCell *)cell {
    CGFloat progress = [self.progresses[@(cell.tag)] floatValue];
    return progress;
}

@end
