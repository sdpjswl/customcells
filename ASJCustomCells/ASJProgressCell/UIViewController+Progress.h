//
//  UIViewController+Progress.h
//  ProgressCell
//
//  Created by sudeep on 02/05/15.
//  Copyright (c) 2015 Sudeep Jaiswal. All rights reserved.
//

@import UIKit;

@class ASJProgressCell;

@interface UIViewController (Progress)

- (void)saveProgressForCell:(ASJProgressCell *)cell;
- (CGFloat)progressForCell:(ASJProgressCell *)cell;

@end
