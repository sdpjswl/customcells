//  ASJCheckboxCell.m
//
// Copyright (c) 2015 Sudeep Jaiswal
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "ASJCheckboxCell.h"

typedef void (^CallbackBlock)(UITableViewCell *cell, BOOL isChecked);

NSTimeInterval const kCellTransitionAnimationDuration = 0.4;

@interface ASJCheckboxCell ()

@property (nonatomic) IBOutlet UIImageView *seachImageView;
@property (nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) UIImage *defaultSearchImage;
@property (weak, nonatomic) UIImage *defaultCheckedImage;
@property (weak, nonatomic) UIImage *defaultUncheckedImage;
@property (copy) CallbackBlock callback;

- (void)initialisations;
- (void)setDefaultImages;
- (void)toggleButtonState;
- (IBAction)checkButtonTapped:(id)sender;

@end

@implementation ASJCheckboxCell

- (void)awakeFromNib {
	// Initialization code
	[self initialisations];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
	// Configure the view for the selected state
}


#pragma mark - Init

- (void)initialisations {
	[self setDefaultImages];
}

- (void)setDefaultImages {
	_seachImageView.image = self.defaultSearchImage;
	[_checkButton setImage:self.defaultUncheckedImage forState:UIControlStateNormal];
	[_checkButton setImage:self.defaultCheckedImage forState:UIControlStateSelected];
}


#pragma mark - IBAction

- (IBAction)checkButtonTapped:(id)sender {
	[self toggleButtonState];
}

- (void)toggleButtonState {
	BOOL selected = !_checkButton.selected;
	_checkButton.selected = selected;
	if (_callback) {
		_callback(self, selected);
	}
}

- (void)checkButtonHandler:(void (^)(UITableViewCell *cell, BOOL isChecked))callback {
	_callback = callback;
}


#pragma mark - Proerty setters

- (void)setSearchImage:(UIImage *)searchImage {
	_seachImageView.image = searchImage;
}

- (void)setUncheckedImage:(UIImage *)uncheckedImage {
	[_checkButton setImage:uncheckedImage forState:UIControlStateNormal];
}

- (void)setCheckedImage:(UIImage *)checkedImage {
	[_checkButton setImage:checkedImage forState:UIControlStateSelected];
}


#pragma mark - Override: Layout and touch

- (void)layoutSubviews{
	[super layoutSubviews];
	self.contentView.frame = CGRectMake(0,
										0,
										self.contentView.frame.size.width,
										self.contentView.frame.size.height);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	[super touchesBegan:touches withEvent:event];
	if (self.editing) {
		[self toggleButtonState];
	}
}


#pragma mark - Override: Editing state

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	[super setEditing:editing animated:NO];
	
	if (editing) {
		_checkButton.hidden = NO;
		[UIView animateWithDuration:kCellTransitionAnimationDuration
							  delay:0.0
							options:UIViewAnimationOptionCurveEaseIn
						 animations:^{
							 _checkButton.alpha = 1.0;
							 _seachImageView.alpha = 0.0;
						 } completion:^(BOOL finished) {
							 _seachImageView.hidden = YES;
						 }];
		return;
	}
	
	_seachImageView.hidden = NO;
	[UIView animateWithDuration:kCellTransitionAnimationDuration
						  delay:0.0
						options:UIViewAnimationOptionBeginFromCurrentState
					 animations:^{
						 _checkButton.alpha = 0.0;
						 _seachImageView.alpha = 1.0;
					 } completion:^(BOOL finished) {
						 _checkButton.hidden = YES;
                         _checkButton.selected = NO;
					 }];
}


#pragma mark - Bundle

+ (NSBundle *)resourcesBundle {
	static NSBundle *resourcesBundle = nil;
	if (!resourcesBundle) {
		NSString *resoucesPath = [[NSBundle mainBundle] pathForResource:@"Resources"
																 ofType:@"bundle"];
		resourcesBundle = [NSBundle bundleWithPath:resoucesPath];
	}
	return resourcesBundle;
}

+ (NSString *)pathForImageNamed:(NSString *)name ofType:(NSString *)type {
	return [[ASJCheckboxCell resourcesBundle] pathForResource:name ofType:type];
}


#pragma mark - Default images

- (UIImage *)defaultSearchImage {
	NSString *path = [ASJCheckboxCell pathForImageNamed:@"search" ofType:@"png"];
	return [UIImage imageWithContentsOfFile:path];
}

- (UIImage *)defaultCheckedImage {
	NSString *path = [ASJCheckboxCell pathForImageNamed:@"checked" ofType:@"png"];
	return [UIImage imageWithContentsOfFile:path];
}

- (UIImage *)defaultUncheckedImage {
	NSString *path = [ASJCheckboxCell pathForImageNamed:@"unchecked" ofType:@"png"];
	return [UIImage imageWithContentsOfFile:path];
}

@end
