# ASJCustomCells

This repository will just contain random custom `UITableViewCell`s and `UICollectionViewCell`s. May prove useful sometime?

###ASJCheckboxCell
A `UITableViewCell` subclass; in editing mode, it shows a checkbox toggle button that can be selected or deselected.

![alt tag](Images/ASJCheckboxCell.png)

###ASJProgressCell
A `UICollectionViewCell` subclass; it has a `UIProgressView` whose progress is saved. You need to handle saving and retreiving progress in your controller class. See example project.

![alt tag](Images/ASJProgressCell.png)

# License

ASJCustomCells is available under the MIT license. See the LICENSE file for more info.