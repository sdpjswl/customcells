//
//  ViewController.m
//  ASJCustomCells
//
//  Created by sudeep on 03/05/15.
//  Copyright (c) 2015 Sudeep Jaiswal. All rights reserved.
//

#import "ViewController.h"

static NSString *const kCellIdentifier = @"cell";

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) IBOutlet UITableView *optionsTableView;
@property (nonatomic) NSArray *options;
@property (nonatomic) NSArray *optionsDetail;

- (void)setUp;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setUp];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // unselect the selected row if any
    NSIndexPath *selection = [_optionsTableView indexPathForSelectedRow];
    if (selection) {
        [_optionsTableView deselectRowAtIndexPath:selection animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Set up

- (void)setUp {
    self.title = @"Select a custom cell";
    _options        = @[@"With checkbox while editing", @"With progress view"];
    _optionsDetail  = @[@"UITableViewCell", @"UICollectionViewCell"];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _options.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kCellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = _options[indexPath.row];
    cell.detailTextLabel.text = _optionsDetail[indexPath.row];
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"CheckboxCellController" sender:self];
            break;
            
        case 1:
            [self performSegueWithIdentifier:@"ProgressCellController" sender:self];
            break;
            
        default:
            break;
    }
}

@end
