//
//  ProgressCellController.m
//  ASJCustomCells
//
//  Created by sudeep on 03/05/15.
//  Copyright (c) 2015 Sudeep Jaiswal. All rights reserved.
//

#import "ProgressCellController.h"
#import "ASJProgressCell.h"
#import "UIViewController+Progress.h"

static NSString *const kCellIdentifier = @"cvcell";

@interface ProgressCellController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic) IBOutlet UICollectionView *logosCollectionView;
@property (nonatomic) NSArray *logoNames;

- (void)setUp;
+ (CGFloat)randomProgress;
+ (NSString *)labelTextForProgress:(CGFloat)progress;

@end

@implementation ProgressCellController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setUp];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Set up

- (void)setUp {
    
    self.title = @"Tap a cell";
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    layout.itemSize = CGSizeMake(110, 110);
    _logosCollectionView.collectionViewLayout = layout;
    
    [_logosCollectionView registerNib:[UINib nibWithNibName:@"ASJProgressCell" bundle:nil] forCellWithReuseIdentifier:kCellIdentifier];
    
    _logoNames = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9",
                   @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9",
                   @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9",
                   @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9",
                   @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9"];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _logoNames.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ASJProgressCell *cell = (ASJProgressCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.tag = indexPath.row;
    cell.anImageView.image = [UIImage imageNamed:_logoNames[indexPath.row]];
    return cell;
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ASJProgressCell *cell = (ASJProgressCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    CGFloat randomProgress = [ProgressCellController randomProgress];
    [cell.aProgressView setProgress:randomProgress animated:YES];
    cell.aLabel.text = [ProgressCellController labelTextForProgress:randomProgress];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(ASJProgressCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat savedProgress = [self progressForCell:cell];
    cell.aProgressView.progress = savedProgress;
    cell.aLabel.text = [ProgressCellController labelTextForProgress:savedProgress];
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(ASJProgressCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    [self saveProgressForCell:cell];
}


#pragma mark - Helpers

+ (CGFloat)randomProgress {
    u_int32_t randomNumberBetween10And100 = (arc4random() % 91) + 10;
    return randomNumberBetween10And100 / 100.0;
}

+ (NSString *)labelTextForProgress:(CGFloat)progress {
    return [NSString stringWithFormat:@"%.0f", progress * 100];
}

@end
