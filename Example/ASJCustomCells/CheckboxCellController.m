//
//  CheckboxCellController.m
//  ASJCustomCells
//
//  Created by sudeep on 03/05/15.
//  Copyright (c) 2015 Sudeep Jaiswal. All rights reserved.
//

#import "CheckboxCellController.h"
#import "ASJCheckboxCell.h"

static NSString *const kCellIdentifier = @"cell";

@interface CheckboxCellController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) IBOutlet UITableView *checkboxTable;
@property (nonatomic) NSArray *dataArray;

- (void)initialisations;
- (void)initTable;

@end

@implementation CheckboxCellController

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.
  [self initialisations];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


#pragma mark - Methods

- (void)initialisations {
  
  self.title = @"Radiohead";
  [self initTable];
  
  UIBarButtonItem *edit = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editTapped:)];
  self.navigationItem.rightBarButtonItem = edit;
}

- (void)editTapped:(id)sender {
  _checkboxTable.editing = !_checkboxTable.editing;
}

- (void)initTable {
  _dataArray = @[@"Pablo Honey",
                 @"The Bends",
                 @"OK Computer",
                 @"Kid A",
                 @"Amnesiac",
                 @"Hail to the Thief",
                 @"In Rainbows",
                 @"The King of Limbs"];
  _checkboxTable.rowHeight = UITableViewAutomaticDimension;
  _checkboxTable.estimatedRowHeight = 44.0;
  [_checkboxTable registerNib:[UINib nibWithNibName:@"ASJCheckboxCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  ASJCheckboxCell *cell = (ASJCheckboxCell *)[tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
  cell.titleLabel.text = _dataArray[indexPath.row];
  [cell checkButtonHandler:^(UITableViewCell *cell, BOOL isChecked) {
    NSLog(@"checked: %d", isChecked);
  }];
  return cell;
}


#pragma mark - UITableViewDelegate

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
  return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
